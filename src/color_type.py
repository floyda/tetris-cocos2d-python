class ColorType:
    Empty = (77, 77, 77, 77)
    Black = (0, 0, 0, 255)
    Red = (255, 0, 0, 255)
    Green = (0, 255, 0, 255)
    Blue = (0, 0, 255, 255)
    Yellow = (255, 255, 0, 255)
    Purple = (255, 0, 255, 255)
    Cyan = (0, 255, 255, 255)
    White = (255, 255, 255, 255)

    ALL = [Red, Green, Blue, Yellow, Purple, Cyan, White]
