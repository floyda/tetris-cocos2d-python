from __future__ import division, print_function, unicode_literals
from cocos.layer import Layer
from color_type import ColorType
from tile import Tile


class Board(Layer):
    def __init__(self, width, height, tile_size, tile_interval):

        super().__init__()

        self._width = width
        self._height = height

        # Create the board
        self._tile_list = []
        self._tile_arr = [[None for _ in range(width)] for _ in range(height)]
        for y in range(self._height):
            for x in range(self._width):
                poxX = tile_interval + x * (tile_size + tile_interval)
                poxY = tile_interval + y * (tile_size + tile_interval)

                tile = Tile(x, y)
                tile.position = poxX, poxY
                self.add(tile, z=0, name=str(x) + ',' + str(y))
                self._tile_arr[y][x] = tile
                self._tile_list.append(tile)

    def get_tile(self, x, y):
        if x < 0 or x >= self._width:
            return None
        if y < 0 or y >= self._height:
            return None
        return self._tile_arr[y][x]

        return None

    def clear(self):
        for tile in self._tile_list:
            tile.set_color()

    def render(self, tile_model):
        self.clear()

        for model in tile_model:
            tile = self.get_tile(model.x, model.y)
            if tile:
                tile.set_color(model.color)
