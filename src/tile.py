from __future__ import division, print_function, unicode_literals
from cocos.cocosnode import CocosNode
from cocos.text import Label
from cocos.layer.util_layers import ColorLayer
from color_type import ColorType
from config import GameConfig


class Tile(CocosNode):
    def __init__(self, x=0, y=0, color=ColorType.Empty):
        self._color = None
        self._board_x = x
        self._board_y = y
        super(Tile, self).__init__()

        self._width = 1
        self._height = 1
        self._color_layer = None

        self.set_color(color)
        self._show_lable()

    def _show_lable(self):
        label = Label('%s-%s' % (self._board_x, self._board_y),
                      font_name='Times New Roman',
                      font_size=10,
                      color=(255, 0, 0, 255),
                      anchor_x='center', anchor_y='center')
        label.position = GameConfig.TILE_SIZE / 2, GameConfig.TILE_SIZE / 2
        self.add(label, z=1, name='label')

    def set_color(self, color=ColorType.Empty):
        if self._color == color:
            return
        self._color = color

        # color
        if self._color_layer is not None:
            self._color_layer.color = color[:3]
        else:
            size = GameConfig.TILE_SIZE
            self._color_layer = ColorLayer(*color, size, size)
            self.add(self._color_layer, z=0, name='color')

        # opacity
        self._color_layer.opacity = color[3]
