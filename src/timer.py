from __future__ import division, print_function, unicode_literals

from cocos.layer import Layer


class Timer(Layer):
    def __init__(self):
        super(Timer, self).__init__()

    def schedule_interval(self, callback, interval, *args, **kwargs):
        super(Timer, self).schedule_interval(
            callback,
            interval,
            *args,
            **kwargs
        )

    def schedule(self, callback, *args, **kwargs):
        super(Timer, self).schedule(
            callback,
            *args,
            **kwargs
        )

    def unschedule(self, callback):
        super(Timer, self).schedule(callback)

    def resume(self):
        super(Timer, self).resume_scheduler()

    def pause(self):
        super(Timer, self).pause_scheduler()
