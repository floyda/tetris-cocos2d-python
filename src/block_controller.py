from __future__ import division, print_function, unicode_literals
from block import Block
from copy import copy


class BlockController:
    __instance = None

    @staticmethod
    def getInstance():
        if BlockController.__instance == None:
            BlockController.__instance = BlockController()
        return BlockController.__instance

    def __init__(self):
        self._block = None

    @property
    def block(self):
        return self._block

    @block.setter
    def block(self, value):
        self._block = value

    def init(self):
        pass

    def create_block(self):
        self._block = Block.random_block()
        return self._block

    def get_tile_model(self):
        return self._block.get_tile_model()

    def create_next_block(self, action):
        new_block = copy(self._block)

        if (action == 'SPACE'):
            new_block.rotate()
        elif (action == 'ENTER'):
            new_block.rotate()
        elif (action == 'UP'):
            new_block.rotate()
        elif (action == 'LEFT'):
            new_block.move_left()
        elif (action == 'RIGHT'):
            new_block.move_right()
        elif (action == 'DOWN'):
            new_block.move_down()
        return new_block
