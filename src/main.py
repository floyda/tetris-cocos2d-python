from __future__ import division, print_function, unicode_literals

import cocos
from game_controller import GameController

import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))


if __name__ == "__main__":
    _director = cocos.director.director

    _director.init(
        width=600,
        height=800,
        caption="Tetris",
        autoscale=False,
        resizable=True
    )
    # _director.set_show_FPS(True)

    main_scene = cocos.scene.Scene()

    gameController = GameController.getInstance()
    gameController.init(main_scene)
    gameController.start()

    _director.run(main_scene)
