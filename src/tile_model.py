from color_type import ColorType


class TileModel(object):
    def __init__(self, x=0, y=0, color=ColorType.Empty):
        self._x = x
        self._y = y
        self._color = color

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value

    def __str__(self):
        return '(%s, %s, %s)' % (self._x, self._y, self._color)

    def __eq__(self, other):
        return self._x == other.x and self._y == other.y and self._color == other.color

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self._x, self._y, self._color))

    def __repr__(self):
        return 'TileModel(%s, %s, %s)' % (self._x, self._y, self._color)
